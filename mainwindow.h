
#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "piemaker.h"
#include "spinbox.h"
#include <QMainWindow>
#include <QWidget>
#include <QtWidgets>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    PieMaker *PieDialog;
    QList<QSpinBox*> boxlist;
    QList<int> boxvalues;
    int total;
    QString fileName;
    QVBoxLayout *boxlayout;
    QHBoxLayout *vertlayout;

private slots:
    void OpenPie();
    void newBox();
    void OpenDir();




};
#endif // MAINWINDOW_H
