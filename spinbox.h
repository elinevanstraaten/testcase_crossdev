#ifndef SPINBOX_H
#define SPINBOX_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QtWidgets>


class SpinBox : public QWidget
{
    Q_OBJECT
public:
    explicit SpinBox(QWidget *parent = nullptr);

signals:

public slots:

private:
    QList<QWidget*> boxlist;
};

#endif // SPINBOX_H
