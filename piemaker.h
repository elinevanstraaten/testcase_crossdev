#ifndef PIEMAKER_H
#define PIEMAKER_H
#include <QPaintEvent>
#include <QPainter>
#include <QWidget>
#include <QDebug>
#include <QTimer>
class PieMaker: public QWidget
{
public:
    PieMaker(QWidget *parent = nullptr);
    void setAmount(QList<int>);
private:
    void paintEvent(QPaintEvent *event);
    int amount1;
    int amount2;
    int amount3;
    int total;
    int percent1;
    int percent2;
    int hoek1;
    int hoek2;
    int count;
    QList<double> percentages;
    QList<int> allvalues;
    QList<double> angles;



};

#endif // PIEMAKER_H
