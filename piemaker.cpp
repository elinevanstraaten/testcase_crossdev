#include "piemaker.h"
# define PI           3.14159265358979323846
#include <iostream>
PieMaker::PieMaker(QWidget *parent) : QWidget(parent)
{

}


void PieMaker::setAmount(QList<int> amounts){
      allvalues = amounts;
      count = 0; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      //Totaal berekenen
      for (int i=0; i < allvalues.size(); i++ ){
          count += allvalues.at(i);

      }
      //Percentages berekenen
      for (int i=0; i < allvalues.size(); i++ ){

         double result = (((double)(allvalues.at(i))/(double)count)*100);
         std::cout << result << std::endl;
         percentages.append(result);
      }
      //Hoeken berekenen
      for (int i=0; i < allvalues.size(); i++ ){
         double angle = (((double)percentages.at(i)/100)*360);
         angles.append(angle);

      }


}

void PieMaker::paintEvent(QPaintEvent *ev)
{


    QPainter per(this);
    QPoint center(this->width()/2,this->height()/2);
    int radius = (qMin(this->width(),this->height()) - 10) / 2;
    QRect rect(center.x()-radius + 30, center.y()-radius, radius*2, radius*2);

    ///Hoeken tekenen///
    float sum = 0;
    srand(20);
    for(int i = 0; i < angles.size(); i++){
        QString perc = QString::number((int)percentages.at(i));
        QString integers = QString::number((int)allvalues.at(i));

        if(i == 0){
            per.setBrush(QColor(rand() % 255,rand() % 255,rand() % 255));
            per.drawPie(rect, 90*16,  angles.at(0) *16);
            per.drawText(20, 10 +(i*0), integers + " - " + perc + "%");
            per.drawRect(5,(i * 30),10,10);
            sum += angles.at(0);

        }
        else {

               per.setBrush(QColor(rand() % 255,rand() % 255,rand() % 255));
               per.drawPie(rect, (90 + sum)*16,  angles.at(i) *16);
               per.drawText(20, 10+ (i*30), integers + " - " + perc + "%");
               per.drawRect(5,(i*30),10,10);

               sum += angles.at(i);

        }
        }

    }
