#ifndef SPINBOXES_H
#define SPINBOXES_H

#include <QMainWindow>
#include <QWidget>
#include <QtWidgets>
#include <QHBoxLayout>

class Spinboxes
{
public:
    Spinboxes(QWidget *parent = nullptr);
private:
     QList<QSpinBox*> boxlist;

};

#endif // SPINBOXES_H
