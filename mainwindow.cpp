#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtWidgets>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    fileName = QDir::currentPath() + "/SaveFile.txt";
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
    connect(ui->Add, SIGNAL(clicked()), this, SLOT(newBox()));
    connect(ui->Submit, SIGNAL(clicked()), this, SLOT(OpenPie()));
    QAction *action = new QAction("&Save as...", this);
    ui->menuSelect_File->addAction(action);
    connect(action, SIGNAL(triggered()), this, SLOT(OpenDir()));
    boxlayout = new QVBoxLayout(ui->groupBox);

    ui->groupBox->setLayout(boxlayout);
    ui->path->setText(fileName);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::OpenPie()
{
    total=0;

    for(int i = 0; i < boxlist.size(); i++){

       boxvalues.append(boxlist.at(i)->value());
       total += boxvalues.at(i);

    }

    QString all = QString::number(total);

    ui->pie->setAmount(boxvalues);
    if(boxvalues.length() > 0){
        ui->stackedWidget->setCurrentIndex(1);
        ui->amount->setText("Total: "+ all + " = 100%");
        ui->label->setText("File is saved in: ");
        ui->menuSelect_File->setDisabled(true);

        QFile f(fileName);

        if (f.open(QIODevice::Append)) {
               QTextStream stream(&f);
               stream << "total = " << total << endl;
               for(int i = 0; i < boxvalues.length();i++){
               stream << "value " << i << ": " << boxvalues.at(i) << endl;
               }
                stream << endl;
         }
       f.close();

    }



}
void MainWindow::newBox(){

      int i;
      vertlayout = new QHBoxLayout();
      boxlayout->addLayout(vertlayout);
      QSpinBox *spinner = new QSpinBox(this);
      QLabel *label = new QLabel(this);
      vertlayout->addWidget(label);
      vertlayout->addWidget(spinner);

      boxlist.append(spinner);
      for (i = 0; i<boxlist.size();i++){
          QString val =  QString::number(i+1);
          label->setText("Value " + val+":");

      }


}
void MainWindow::OpenDir(){

    fileName = QFileDialog::getSaveFileName(this, tr("Save Text File"),"C:\\", tr("Text Files (*.txt)"));
    ui->path->setText(fileName);

}




